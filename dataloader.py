import pyvips
import os
import torch
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import random


class PyvipsDataset(Dataset):
    """Face Landmarks dataset."""

    def __init__(self, root_image, n_samples, cropsize, transform=None):
        """
        Args:
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.transform = transform
        self.pyvipsImage = pyvips.Image.new_from_file(root_image)
        self.height = self.pyvipsImage.height
        self.width = self.pyvipsImage.width
        self.n_samples = n_samples
        self.cropsize = cropsize
        self.sample_list = self._get_n_random_samples(self.n_samples, self.cropsize)

    def __len__(self):
        return self.n_samples

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        return self.sample_list[idx]

    def _get_n_random_samples(self, n,cropsize):

        sample_list = []

        for i in range(n):
            x = random.randint(0, self.width - self.cropsize)
            y = random.randint(0, self.height - self.cropsize)
            sample = self.pyvipsImage.crop(x, y, self.cropsize, self.cropsize)
            if self.transform:
                sample = self.transform(sample)
            sample_list.append(sample)

        return sample_list


def clean_up_dir(dir):
    for f in os.listdir(dir):
        os.remove(os.path.join(dir, f))


# def main(image_path, cropsize=256, overlap=0, out_dir='out'):

#     clean_up_dir(out_dir)

#     # load source image
#     pyvipsImage = pyvips.Image.new_from_file(image_path)

#     height = pyvipsImage.height
#     width = pyvipsImage.width

#     dx = int((1. - overlap) * cropsize)
#     dy = int((1. - overlap) * cropsize)

#     i = 0
#     for y0 in range(0, height, dy):
#         for x0 in range(0, width, dx):

#             # make sure we don't have a tiny image on the edge
#             if y0 + cropsize > height:
#                 y = height - cropsize
#             else:
#                 y = y0
#             if x0 + cropsize > width:
#                 x = width - cropsize
#             else:
#                 x = x0

#             # extract image
#             buffer_tile = pyvipsImage.crop(x, y, cropsize, cropsize)
#             buffer_tile.write_to_file(os.path.join(out_dir, f"{x}_{y}.tif"))

#         i+=1
#         print(f"{i}/{len(range(0, height, dy))}", end="\r")
#         if i % 500 == 0:
#             time.sleep(5)

#     return


# if __name__ == "__main__":

#     # main('../test.tif')
#     print("MNT")
#     main('../MNT_PILOT_SITE.tif', out_dir='MNT')
#     time.sleep(10)
#     print("ORTHO")
#     main('../ORTHO_PILOT_SITE_2021_11.tif', out_dir='ORTHO')
#     time.sleep(10)
#     print("RGBNIR")
#     main('../RGBNIR_FUSION.tif',out_dir='RGBNIR')
