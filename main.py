from dataloader import PyvipsDataset
import torch


if __name__ == "__main__":

    dataset = PyvipsDataset('ressources/test.tif',10,20)

    dataset_loader = torch.utils.data.DataLoader(dataset,
                                            batch_size=4, shuffle=True,
                                            num_workers=4)
